# GitLab 
## What
GitLab is a web-based DevOps lifecycle tool that provides a Git-repository manager providing wiki, issue-tracking and CI/CD pipeline features

GitLab Server: Everything you do will be saved in a db (repository, wiki, jobs, pipeline,...) and it manages the entire process (make sure runner is picking up this job,..)

GitLab Runner: where to run GitLab pipeline
---
## Why
* Simple, scalable architecture
* Docker first approach
* Pipeline as Code
* Merge Requests with CI support