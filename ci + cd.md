# Continuous Integration (CI)
## What
* Practice of continuously integrating code changes
* Ensures that the project can still be built/compiled
* Ensures that changes pass all tests, guidelines, and code compliance standards 

## CI Pipeline 
```
BUILD -> CODE QUALITY -> TESTS -> PACKAGE
```
## Why (Advantages) 
* Errors are detected early in the development process
* Reduces integration problems 
* Allow developers to work faster

# Continuous Delivery (CI)
* Ensures that the software can be deployed anytime to production
* Commonly, the latest version is deployed to a testing/staging system 

## CDelivery Pipeline
```
PACKAGE -> REVIEW/TEST -> TESTING/STAGING -> CODE REVIEW/PRODUCTION
```

## CDeployment Pipeline
```
PACKAGE -> REVIEW/TEST -> TESTING/STAGING -> PRODUCTION
```

## Why (Advantages)
* Ensures that every change is releasable by testing that it can be deployed
* Reduced risk of a new deployment 
* Delivers value much faster 