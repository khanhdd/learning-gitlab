# Artifacts

* Usually the output of a build tool
* Designed to save some compiled/generated part of the build
* Pass data between stages/jobs

# Cache

* Not to be used to store build results
* Should only be used as a temporary storage for project dependencies
